# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Author: Jean-Pierre Hickey, University of Waterloo
# Info: This script reads in the postprocessed, scaled mean profile from
#       generated from compute_profile.py (which postprocesses the DNS) and
#       generates a comparative figure with the analytical law of the wall.
#
# Please cite this work as:
#  Pan, Z., Zhang, Y., Gustavsson, J.P.R., Hickey, J.-P. and Cattafesta III, L. N. Unscented Kalman filter (UKF) based nonlinear parameter estimation for a turbulent boundary layer: a data assimilation framework
#  Meas. Sci. and Technol.
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Requirements: matplotlib and numpy
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import matplotlib.pyplot as plt
import numpy as np

#--------------------------------------
# Read the postprocessed near wall velocity profiles
folder="./postProcessedProfiles/"
data1=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0076197.dat')
data2=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0078374.dat')
data3=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0091436.dat')
data4=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0093612.dat')
data5=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0095789.dat')
data6=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_0113206.dat')
data8=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_n0060957.dat')
data9=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_n0087082.dat')
data10=np.loadtxt(folder+'nearwall_M03_noHeating_bottomWall_n0106674.dat')

#--------------------------------------
# Compute the average scaled and unscaled velocity profile (we somewhat unnecessarily compute the ymean as well, the grid doesnt change but the scaled y value may change slightly.)
umean=data1[:,1]+data2[:,1]+data3[:,1]+data4[:,1]+data5[:,1]+data6[:,1]+data8[:,1]+data9[:,1]+data10[:,1]
ubar=data1[:,2]+data2[:,2]+data3[:,2]+data4[:,2]+data5[:,2]+data6[:,2]+data8[:,2]+data9[:,2]+data10[:,2]
ymean=data1[:,0]+data2[:,0]+data3[:,0]+data4[:,0]+data5[:,0]+data6[:,0]+data8[:,0]+data9[:,0]+data10[:,0]

numberOfDataSets=9.
ubar/=numberOfDataSets
umean/=numberOfDataSets
ymean/=numberOfDataSets

#--------------------------------------
# Plot the mean velocity profile and compare with law of the wall
plt.figure()
plt.semilogx(ymean, umean,lw=3, label='DNS')
ystar = np.logspace(-0.5,3)
u_tau=1./0.41*np.log(ystar)+5.3
plt.scatter(ystar,u_tau,color='red', label='law of the wall')
plt.scatter(ystar[:25],ystar[:25],color='red')
plt.xlabel(r'$y^+$')
plt.ylabel(r'$U^+$')
plt.legend()
plt.savefig('./figures/DNS_law_of_the_wall.pdf')
plt.show()

# DNS-post-processing

PACKAGE NAME: DNS-post-processing

DESCRIPTION: This repository contains the raw data (9 snapshots) of a turbulent channel flow at a bulk Mach number of 0.3 and a Re_tau of about 750. Post-processing scripts are also provided. This data was used for the analysis of the following paper:
 Pan, Z., Zhang, Y., Gustavsson, J.P.R., Hickey, J.-P. and Cattafesta III, L. N. Unscented Kalman filter (UKF) based nonlinear parameter estimation for a turbulent boundary layer: a data assimilation framework,  Meas. Sci. and Technol. 2020.

SIMULATION DETAILS
  The raw DNS data can be found here: https://doi.org/10.20383/101.0222.
  Turbulent channel flow with stream- and spanwise periodic domain. A body force is used to sustain the channel flow. The bulk momentum of the flow is unity and bulk Mach number is 0.3. A sixth-order central finite difference scheme for spatial derivatives and a fourth order explicit Runge-Kutta scheme in time. Details about the code can be found in the references of the Pan et al. (2020) paper. The mesh is 512x512x340 with wall clustering. The domain size is 2pi x 2 x pi. The small size of the domain was assessed by previous works (Lozano and Jimenez, 2014). This small domain size results in a greater variability between the snapshots. For the simulations, we assume gamma=1.4, R=7.9365  (where R = 1/(gamma*Mach^2)) and Pr=0.7. The viscosity is mu= 0.00007143 and a 3/4th power law is used for the viscosity variation with temperature. Additional details can be found in Pan et al. (2020).


FILES:     compute_profile.py
           plot_law_of_the_wall.py
           ioHybrid.py

DATA FOLDERS:
           ./postProcessedProfiles/
           ./figs/

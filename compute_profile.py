# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Author: Jean-Pierre Hickey, University of Waterloo
# Info: This script reads an individual raw DNS snapshot and outputs the
#       scaled and unscaled near wall velocity profiles. This code can
#       also be trivially extended to compute useful statistics
#
#
# Please cite this work as:
#  Pan, Z., Zhang, Y., Gustavsson, J.P.R., Hickey, J.-P. and Cattafesta III, L. N. Unscented Kalman filter (UKF) based nonlinear parameter estimation for a turbulent boundary layer: a data assimilation framework
#  Meas. Sci. and Technol.
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#  Requirements: ioHybrid, matplotlib and numpy
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import ioHybrid as io
import numpy as np
import matplotlib.pyplot as plt

#--------------------------------------
# User input data
folder  = "./rawData/"
file    = "R7000Fine_n0087082_t1.000e+01.res"

#--------------------------------------
# Constants from the simulation
muRef = 0.00007143   #viscosity
R     = 7.9365       # universal gas constant

#--------------------------------------
# Read binary file and computes primitive values
x,y,z,field,var=io.binary_io(folder+file)
Nx=np.size(x)
Ny=np.size(y)
Nz=np.size(z)
Ntot=Nx*Ny*Nz
Rho=np.reshape(field[0:Ntot],[Nx,Ny,Nz])
U=np.reshape(field[Ntot:Ntot*2],[Nx,Ny,Nz])/Rho
V=np.reshape(field[Ntot*2:Ntot*3],[Nx,Ny,Nz])/Rho
W=np.reshape(field[Ntot*3:Ntot*4],[Nx,Ny,Nz])/Rho

#--------------------------------------
# Generates empty arrays for mean quantities
Ubar=np.zeros(Ny)
Vbar=np.zeros(Ny)
Wbar=np.zeros(Ny)
Rhobar=np.zeros(Ny)
for jj in range(Ny):
  Ubar[jj]  = np.mean(U[:,jj,:])
  Vbar[jj]  = np.mean(V[:,jj,:])
  Wbar[jj]  = np.mean(W[:,jj,:])
  Rhobar[jj]= np.mean(Rho[:,jj,:])

#--------------------------------------
# Compute useful statistics
dudy_w_bot = Ubar[0]/(1.0-abs(y[0]))
mu_bot = muRef
rho_bot=Rhobar[0]
tau_w_bot=(mu_bot*dudy_w_bot)
u_tau_bot = (tau_w_bot/rho_bot)**0.5
ystar_bot=u_tau_bot/(mu_bot/rho_bot)
ystar = np.logspace(-0.5,3)
u_tau=1./0.41*np.log(ystar)+5.3

#--------------------------------------
# Compute the scaled velocity profile and save
output="./postProcessedProfiles/"
head_bot="BOTTOM WALL M_bulk=0.3: utau="+str(u_tau_bot)+" tau="+str(tau_w_bot)+" \n y+, u+, bar(u)"
np.savetxt(output+'nearwall_M03_bottomWall.txt',np.column_stack(((1+y)*ystar_bot,Ubar/u_tau_bot,Ubar)),header=head_bot)
